/*Multiple Components in Sync  Lifting State Up */
/*import React from 'react';

//Stateless Functional Component
const Navbar = ({totalCounters}) => {
    console.log('Navbar - Rendered');
    return ( 
        <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand">Navbar{" "}
                <span 
                    className="badge badge-pill badge-secondary">
                    {totalCounters}</span>
            </a> 
        </nav>
    );
};

 
export default Navbar;*/

import React from 'react';

//Stateless Functional Component
const Navbar = props => {
    console.log('Navbar - Rendered');
    return ( 
        <nav className="navbar navbar-light bg-light">
            <p className="navbar-brand">Navbar{" "}
                <span 
                    className="badge badge-pill badge-secondary">
                    {props.totalCounters}</span>
            </p> 
        </nav>
    );
};

 
export default Navbar;