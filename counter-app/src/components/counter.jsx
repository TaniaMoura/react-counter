/*Embedding Expressions*/
/*import React, { Component } from 'react';

class Counter extends Component {
    state = {
        count: 0
    };
    render() { 
        return (
            <div>
                <span>{this.formatCount()}</span>
                <button>Increment</button>
            </div>
        );
    }

    formatCount() {
        const { count } = this.state; 
        return count === 0 ? 'Zero' : count;
    }
}
 
export default Counter;*/


/*Setting Atrributes*/ 
/*import React, { Component } from 'react';

class Counter extends Component {
    state = {
        count: 0,
       imageUrl: 'https://picsum.photos/200'
    };

    render() { 
        return (
            <div>
                <img src= {this.state.imageUrl} alt='' />
                <span className='badge badge-primary m-2'>{this.formatCount()}</span>
                <button className='btn btn-secondary btn-sm'>Increment</button>
            </div>
        );
    }

    formatCount() {
        const { count } = this.state; 
        return count === 0 ? 'Zero' : count;
    }
}
 
export default Counter;*/


/*Rendering Classes Dynamically*/ 
/*import React, { Component } from 'react';

class Counter extends Component {
    state = {
        count: 1
    };

    render() { 
        return (
            <div>
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button className='btn btn-secondary btn-sm'>Increment</button>
            </div>
        );
    }

    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += (this.state.count === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { count } = this.state; 
        return count === 0 ? 'Zero' : count;
    }
}
 
export default Counter;*/


/*Rendering Lists*/
/*import React, { Component } from 'react';

class Counter extends Component {
    state = {
        count: 0,
        tags: ['tag1', 'tag2', 'tag3']
    };

    render() { 
        return (
            <div>
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button className='btn btn-secondary btn-sm'>Increment</button>
                <ul>
                    {this.state.tags.map(tag => <li key={tag}>{tag}</li>)}
                </ul>
            </div>
        );
    }

    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += (this.state.count === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { count } = this.state; 
        return count === 0 ? 'Zero' : count;
    }
}
 
export default Counter;*/


/*Conditional Rendering*/
/*import React, { Component } from 'react';

class Counter extends Component {
    state = {
        count: 0,
        tags: []
    };

    renderTags() {
        if (this.state.tags.length === 0) return <p>There are no tags!</p>;

        return <ul>{this.state.tags.map(tag => <li key={tag}>{tag}</li>)}</ul>;
    }


    render() { 
        return (
            <div>
                {this.state.tags.length === 0 && 'Please create a new tag!'}
                {this.renderTags()}
            </div>
        );
    }
}
 
export default Counter;*/


/*Handling Events*/
/*import React, { Component } from 'react';

class Counter extends Component {
    state = {
        count: 0
    };

    handleIncrement() {
        console.log('Increment Clicked');
    }

    render() { 
        return (
            <div>
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button onClick={this.handleIncrement} className='btn btn-secondary btn-sm'>Increment</button>
            </div>
        );
    }

    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += (this.state.count === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { count } = this.state; 
        return count === 0 ? 'Zero' : count;
    }
}
 
export default Counter;*/


/*Binding Event Handlers*/
/*import React, { Component } from 'react';

class Counter extends Component {
    state = {
        count: 0
    };

    //constructor() {
        //super();
        //this.handleIncrement = this.handleIncrement.bind(this);
    //}

    handleIncrement = () => {
        console.log('Increment Clicked', this);
        //obj.method();
        //function();
    }

    render() { 
        return (
            <div>
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button onClick={this.handleIncrement} className='btn btn-secondary btn-sm'>Increment</button>
            </div>
        );
    }

    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += (this.state.count === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { count } = this.state; 
        return count === 0 ? 'Zero' : count;
    }
}
 
export default Counter;*/


/*Updating the State*/
/*import React, { Component } from 'react';

class Counter extends Component {
    state = {
        count: 0
    };

    //constructor() {
        //super();
        //this.handleIncrement = this.handleIncrement.bind(this);
    //}

    handleIncrement = () => {
       this.setState({count: this.state.count + 1})
    };

    render() { 
        return (
            <div>
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button onClick={this.handleIncrement} className='btn btn-secondary btn-sm'>Increment</button>
            </div>
        );
    }

    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += (this.state.count === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { count } = this.state; 
        return count === 0 ? 'Zero' : count;
    }
}
 
export default Counter;*/



/*Passing Event Arguments*/
/*import React, { Component } from 'react';

class Counter extends Component {
    state = {
        count: 0
    };

    handleIncrement = product => {
        console.log(product);
        this.setState({count: this.state.count + 1});
    };
 
    //<button onClick={() => this.handleIncrement(product)}........
    render() { 
        return (
            <div>
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button onClick={() => this.handleIncrement({id: 1})} className='btn btn-secondary btn-sm'>Increment</button>
            </div>
        );
    }

    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += (this.state.count === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { count } = this.state; 
        return count === 0 ? 'Zero' : count;
    }
}
 
export default Counter;*/


/*Passing Children*/
/*import React, { Component } from 'react';

class Counter extends Component {
    state = {
        value: this.props.value
    };

    handleIncrement = () => {
        this.setState({value: this.state.value + 1});
    };
 
    render() { 
        return (
            <div>
                {this.props.children}
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button onClick={this.handleIncrement} className='btn btn-secondary btn-sm'>Increment</button>
            </div>
        );
    }

    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += (this.state.value === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { value } = this.state; 
        return value === 0 ? 'Zero' : value;
    }
}
 
export default Counter;*/


/*Updating the State*/
/*import React, { Component } from 'react';

class Counter extends Component {
    state = {
        value: this.props.counter.value
    };

    handleIncrement = () => {
        this.setState({value: this.state.value + 1});
    };
 
    render() { 
        return (
            <div>
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button onClick={this.handleIncrement} className='btn btn-secondary btn-sm'>Increment</button>
                <button onClick={() => this.props.onDelete(this.props.counter.id)} className="btn btn-danger btn-sm m-2">Delete</button>
            </div>
        );
    }

    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += (this.state.value === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { value } = this.state; 
        return value === 0 ? 'Zero' : value;
    }
}
 
export default Counter;*/


/*Single Source of Truth*/
/*import React, { Component } from 'react';

class Counter extends Component {
    state = {
        value: this.props.counter.value
    };

    handleIncrement = () => {
        this.setState({value: this.state.value + 1});
    };
 
    render() { 
        return (
            <div>
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button onClick={this.handleIncrement} className='btn btn-secondary btn-sm'>Increment</button>
                <button onClick={() => this.props.onDelete(this.props.counter.id)} className="btn btn-danger btn-sm m-2">Delete</button>
            </div>
        );
    }

    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += (this.state.value === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { value } = this.state; 
        return value === 0 ? 'Zero' : value;
    }
}
 
export default Counter;*/


/*Removing the Local State*/
/*import React, { Component } from 'react';

class Counter extends Component {
    render() { 
        return (
            <div>
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button 
                    onClick={() => this.props.onIncrement(this.props.counter)} 
                    className='btn btn-secondary btn-sm'>Increment</button>
                <button 
                    onClick={() => this.props.onDelete(this.props.counter.id)} 
                    className="btn btn-danger btn-sm m-2">Delete</button>
            </div>
        );
    }

    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += (this.props.counter.value === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { value } = this.props.counter; 
        return value === 0 ? 'Zero' : value;
    }
}
 
export default Counter;*/


/*Multiple Components in Sync  Lifting State Up */
import React, { Component } from 'react';

class Counter extends Component {
    componentDidUpdate(prevProps, prevState) {
        console.log('prevProps', prevProps);
        console.log('prevState', prevState);
        if (prevProps.counter.value !== this.props.counter.value) {
            //Ajax Call and get new data from the server
        }
    }

    componentWillUnmount() {
        console.log("Counter - Unmount");
    }

    render() { 
        console.log("Counter - Rendered");
        return (
            <div>
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button 
                    onClick={() => this.props.onIncrement(this.props.counter)} 
                    className='btn btn-secondary btn-sm'>Increment</button>
                <button 
                    onClick={() => this.props.onDelete(this.props.counter.id)} 
                    className="btn btn-danger btn-sm m-2">Delete</button>
            </div>
        );
    }

    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += (this.props.counter.value === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { value } = this.props.counter; 
        return value === 0 ? 'Zero' : value;
    }
}
 
export default Counter;
