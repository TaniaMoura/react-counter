/*Composing Components*/
/*import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {
    state = { 
        counters: [
            {id: 1, value: 0},
            {id: 2, value: 0},
            {id: 3, value: 0},
            {id: 4, value: 0}
        ]
     };
    render() { 
        return ( 
            <div>
                {this.state.counters.map(counter => <Counter key={counter.id} />)}
            </div> 
        );
    }
}
 
export default Counters;*/


/*Passing Data to Components*/
/*import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {
    state = { 
        counters: [
            {id: 1, value: 0},
            {id: 2, value: 0},
            {id: 3, value: 0},
            {id: 4, value: 0}
        ]
     };
    render() { 
        return ( 
            <div>
                {this.state.counters.map(counter => (
                    <Counter key={counter.id} value={counter.value} />))}
            </div> 
        );
    }
}
 
export default Counters;*/


/*Passing Children*/
/*import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {
    state = { 
        counters: [
            {id: 1, value: 4},
            {id: 2, value: 0},
            {id: 3, value: 0},
            {id: 4, value: 0}
        ]
     };
    render() { 
        return ( 
            <div>
                {this.state.counters.map(counter => ( 
                    <Counter key={counter.id} value={counter.value}>
                        <h4>Counter #{counter.id}</h4>
                    </Counter>
                    ))}
            </div> 
        );
    }
}
 
export default Counters;*/


/*Updating the State*/
/*import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {
    state = { 
        counters: [
            {id: 1, value: 4},
            {id: 2, value: 0},
            {id: 3, value: 0},
            {id: 4, value: 0}
        ]
     };

     handleDelete = (counterId) => {
         //console.log('Event Handler Called', counterId);
         const counters = this.state.counters.filter(c => c.id !== counterId);
         this.setState({counters});
     };

    render() { 
        return ( 
            <div>
                {this.state.counters.map(counter => ( 
                    <Counter 
                        key={counter.id} 
                        onDelete={this.handleDelete} 
                        counter={counter} />
                    ))}
            </div> 
        );
    }
}
 
export default Counters;*/


/*Single Source of Truth*/
/*import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {
    state = { 
        counters: [
            {id: 1, value: 4},
            {id: 2, value: 0},
            {id: 3, value: 0},
            {id: 4, value: 0}
        ]
     };

     handleReset = () => {
        const counters = this.state.counters.map(c => {
            c.value = 0;
            return c;
        });
        this.setState({counters});
     };

     handleDelete = (counterId) => {
         //console.log('Event Handler Called', counterId);
         const counters = this.state.counters.filter(c => c.id !== counterId);
         this.setState({counters});
     };

    render() { 
        return ( 
            <div>
                <button 
                    onClick={this.handleReset}
                    className="btn btn-primary btn-sm m-2">Reset</button>
                {this.state.counters.map(counter => ( 
                    <Counter 
                        key={counter.id} 
                        onDelete={this.handleDelete} 
                        counter={counter} />
                    ))}
            </div> 
        );
    }
}
 
export default Counters;*/


/*Removing the Local State*/
/*import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {
    state = { 
        counters: [
            {id: 1, value: 4},
            {id: 2, value: 0},
            {id: 3, value: 0},
            {id: 4, value: 0}
        ]
     };

     handleIncrement = counter => {
         //console.log(counter);
         const counters = [...this.state.counters];
         const index = counters.indexOf(counter);
         counters[index] = {...counter};
         counters[index].value++;
         this.setState({counters});
         //console.log(this.state.counters[0]);
     };

     handleReset = () => {
        const counters = this.state.counters.map(c => {
            c.value = 0;
            return c;
        });
        this.setState({counters});
     };

     handleDelete = (counterId) => {
         //console.log('Event Handler Called', counterId);
         const counters = this.state.counters.filter(c => c.id !== counterId);
         this.setState({counters});
     };

    render() { 
        return ( 
            <div>
                <button 
                    onClick={this.handleReset}
                    className="btn btn-primary btn-sm m-2">Reset</button>
                {this.state.counters.map(counter => ( 
                    <Counter 
                        key={counter.id} 
                        onDelete={this.handleDelete} 
                        onIncrement={this.handleIncrement}
                        counter={counter} />
                    ))}
            </div> 
        );
    }
}
 
export default Counters;*/


/*Multiple Components in Sync  Lifting State Up */
/*import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {

    render() { 
        const {onReset, counters, onDelete, onIncrement} = this.props;

        return ( 
            <div>
                <button 
                    onClick={onReset}
                    className="btn btn-primary btn-sm m-2">Reset</button>
                {counters.map(counter => ( 
                    <Counter 
                        key={counter.id} 
                        onDelete={onDelete} 
                        onIncrement={onIncrement}
                        counter={counter} />
                    ))}
            </div> 
        );
    }
}
 
export default Counters;*/

/*Multiple Components in Sync  Lifting State Up */
import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {

    render() { 
        console.log("Counters - Rendered");

        return ( 
            <div>
                <button 
                    onClick={this.props.onReset}
                    className="btn btn-primary btn-sm m-2">Reset</button>
                {this.props.counters.map(counter => ( 
                    <Counter 
                        key={counter.id} 
                        onDelete={this.props.onDelete} 
                        onIncrement={this.props.onIncrement}
                        counter={counter} />
                    ))}
            </div> 
        );
    }
}
 
export default Counters;